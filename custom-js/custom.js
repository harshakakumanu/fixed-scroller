var app = angular.module('app', []);

//app.controller('mainCtrl', function ($scope) {
//
//});

//Jquery function for sticking the div on scroll.
$(function() {
	var offset = $("#hp-boxid").offset();
	var topPadding = 15;
	$( document ).ready(function() {
		$("#in-page-helpid").height( $(window).height()-400);
		if ($(window).scrollTop() > offset.top) {

			$("#hp-boxid").stop().css({

				marginTop: $(window).scrollTop() - offset.top + topPadding

			});


		}
	});




	$(window).scroll(function() {
		if ($(window).scrollTop() > offset.top) {
			$("#in-page-helpid").height( $(window).height()-100);
			$("#hp-boxid").stop().css({

				marginTop: $(window).scrollTop() - offset.top + topPadding

			});


		} else {
			$("#in-page-helpid").height( $(window).height()-300);
			$("#hp-boxid").stop().css({

				marginTop: 0

			});

		}
		//$(".scroll").bind( 'mousewheel DOMMouseScroll', function ( e ) {
		//	//Get the original Event
		//	var e0 = e.originalEvent,
		//	//Hold the movement of the scroll
		//		delta = e0.wheelDelta ;
		//	//If it's negative add -30 for each step or 30 if is positive
		//	this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
		//	//Apply the scroll only for the element with the
		//	//handler
		//	e.preventDefault();
		//	//Prevent the normal event
		//});

	});
	$(window).resize(function() {
		setInterval(function () {
			if ($(window).scrollTop() > offset.top) {

				$("#in-page-helpid").height($(window).height() - 100);
			}
			else{
				$("#in-page-helpid").height($(window).height() - 300);
			}
		}, 300);

	});


});




//PRE-EXISTING SCRIPT CODE

// START - Disable nav-pills ( .nav-pills | data-toggle=pill )
// Disable any link where the li has a class of disabled in the .nav-pills
$('.nav-pills li.disabled > a[data-toggle=pill]').on('click', function(e) {
	e.stopImmediatePropagation();
});
// STOP - Disable nav-ills ( .nav-pills | data-toggle=pill )

// START - Scrolling help ( #in-page-helpid .in-pg-help )
// The complete function for the scrolling help. We shall use only the necessary settings.
(function($) {
	$(window).load(function() {

		/* all available option parameters with their default values */
		$("#in-page-helpid").mCustomScrollbar({
			//setWidth:false,
			//setHeight:false,
			//setTop:0,
			//setLeft:0,
			axis: "y",
			scrollbarPosition: "inside",
			scrollInertia: 950,
			autoDraggerLength: true,
			autoHideScrollbar: false,
			autoExpandScrollbar: true,
			alwaysShowScrollbar: 0,
			snapAmount: null,
			snapOffset: 0,
			mouseWheel: {
				enable: true,
				scrollAmount: "auto",
				axis: "y",
				preventDefault: false,
				deltaFactor: "auto",
				normalizeDelta: false,
				invert: false
			},
			scrollButtons: {
				enable: true,
				scrollType: "stepless",
				scrollAmount: "auto"
			},
			keyboard: {
				enable: true,
				scrollType: "stepless",
				scrollAmount: "auto"
			},
			contentTouchScroll: 25,
			advanced: {
				autoExpandHorizontalScroll: false,
				autoScrollOnFocus: "input,textarea,select,button,datalist,keygen,a[tabindex],area,object,[contenteditable='true']",
				updateOnContentResize: true,
				updateOnSelectorLength: false
			},
			theme: "dark-thick",
			callbacks: {
				onScrollStart: false,
				onScroll: false,
				onTotalScroll: false,
				onTotalScrollBack: false,
				whileScrolling: false,
				onTotalScrollOffset: 0,
				onTotalScrollBackOffset: 0,
				alwaysTriggerOffsets: true
			},
			live: false
		});

	});
})(jQuery);
// STOP - Scrolling help ( #in-page-helpid .in-pg-help )

// START - Show/hide scrolling help
// TBA
// STOP - Show/hide scrolling help

// START - Highlight help elemetns
$(window).load(function() {
	setupHelpLinksAndScrollbar();
});

var setupHelpLinksAndScrollbar = function() {
	if ($(".in-pg-help").length > 0) {

		var highlightHelpBlock = function(event, component) {
			event.preventDefault();
			var selector = "#" + component.parent().attr("helpid");
			$(".in-pg-help").mCustomScrollbar("scrollTo", selector);
			$(".hp-item-selected").removeClass("hp-item-selected");
			$(".h-selected").removeClass("h-selected");
			$(selector).addClass("hp-item-selected");
			// alert(selector);
		};

		// when labels with a "?" are clicked it will highlight the corresponding help section
		$(".wrd-inln-hlp").unbind("click").bind("click", function(event) {
			highlightHelpBlock(event, $(this));
			$(this).addClass("h-selected");
		});
	}
};
// STOP - Highlight help elemetns

// START - Simple txt spinner
var spins = [
	"←↖↑↗→↘↓↙",
	"▁▃▄▅▆▇█▇▆▅▄▃",
	"▉▊▋▌▍▎▏▎▍▌▋▊▉",
	"▖▘▝▗",
	"┤┘┴└├┌┬┐",
	"◢◣◤◥",
	"◰◳◲◱",
	"◴◷◶◵",
	"◐◓◑◒",
	"|/-\\",
	".oO@*",
	["◡", "⊙", "◠"],
	"⣾⣽⣻⢿⡿⣟⣯⣷",
	"⠁⠂⠄⡀⢀⠠⠐⠈",
	[">))'>"," >))'>","  >))'>","   >))'>","    >))'>","   <'((<","  <'((<"," <'((<"],
	["checking •     ","checking ••    ", "checking •••   ","checking  •••  ","checking   ••• ","checking    •• ","checking    • ","checking       "],
];

var spin = spins[15],
	title$ = $('#spinner'),
	i = 0;

setInterval(function() {
	i = i == spin.length - 1 ? 0 : ++i;
	title$.text(spin[i]);
}, 300);
// STOP - Simple txt spinner
